FROM nginx:1.24.0-bullseye-perl

COPY index.html /usr/share/nginx/html
 EXPOSE 80
 CMD ["nginx", "-g", "daemon off;"]
